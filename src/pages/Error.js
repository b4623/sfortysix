import { Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function ErrorPage(){
		return(
				<Row>
					<Col className = 'p-5'>
					<h1>404-Not Found</h1>
					<p>The Page Your Looking Cannot Be Found</p>
					<Button variant= "primary" as={Link} to="/">Back Home</Button>
					</Col>
				</Row>		
				)
}