import {Row , Col, Card} from 'react-bootstrap';


export default function Highlights(){
	return (
		<Row>
			<Col>
				<Card className='cardHighlight p-3s'>
					<Card.Body>
						<Card.Title>Learn From Home</Card.Title>
						<Card.Text>Lorem ipsum, dolor, sit amet consectetur adipisicing elit. Vero atque quidem aliquam! Labore enim aliquam nostrum sit sunt iure voluptas porro sed hic architecto cupiditate, quisquam pariatur! Hic, asperiores excepturi.</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col>
				<Card className='cardHighlight p-3s'>
					<Card.Body>
						<Card.Title>Study Now, Pay Later</Card.Title>
						<Card.Text>Lorem ipsum, dolor, sit amet consectetur adipisicing elit. Vero atque quidem aliquam! Labore enim aliquam nostrum sit sunt iure voluptas porro sed hic architecto cupiditate, quisquam pariatur! Hic, asperiores excepturi.</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col>
				<Card className='cardHighlight p-3s'>
					<Card.Body>
						<Card.Title>Be Part of Our Community</Card.Title>
						<Card.Text>Lorem ipsum, dolor, sit amet consectetur adipisicing elit. Vero atque quidem aliquam! Labore enim aliquam nostrum sit sunt iure voluptas porro sed hic architecto cupiditate, quisquam pariatur! Hic, asperiores excepturi.</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	



		)
}